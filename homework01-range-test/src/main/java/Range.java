import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by igors on 15.06.16.
 */
@Data
public class Range {

    private long lowerBound;
    private long upperBound;
    private List<Long> items;

    public Range(long lowerBound, long upperBound, List<Long> items) {
        if (lowerBound >= upperBound) {
            throw new RuntimeException("Upper bound is less or equals lower bound");
        }
        this.lowerBound = lowerBound;
        this.upperBound = upperBound;
        this.items = items;
    }

    boolean isBefore(Range otherRange){
        return this.lowerBound > otherRange.getUpperBound();
    }

    boolean isAfter(Range otherRange){
        return this.upperBound<otherRange.lowerBound;
    }

    boolean isConcurrent(Range otherRange){
        if ((this.lowerBound <=otherRange.upperBound)&&(this.upperBound>=otherRange.upperBound)){
            return true;
        }
        if ((this.lowerBound >= otherRange.lowerBound)&&(this.upperBound<=otherRange.upperBound)){
            return true;
        }
        if((this.lowerBound<=otherRange.lowerBound)&&(this.upperBound>=otherRange.lowerBound)){
            return true;
        }
        return false;
    }

    boolean contains(long value){
        return value>=this.lowerBound && value<=this.upperBound;
    }

    List<Long> asList(){
        return items;
    }

    Iterator<Long> asIterator(){
        return items.iterator();
    }
}
