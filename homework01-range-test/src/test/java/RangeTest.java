import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;

import static org.hamcrest.Matchers.not;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.*;

/**
 * Created by igors on 15.06.16.
 */
public class RangeTest {

    private Range range;


    @Before
    public void init() {
        range = new Range(10,50,new ArrayList<Long>(){{add(10l);add(20l);add(30l);add(40l);add(50l);}});
    }

    @Test
    public void isBefore() throws Exception {
        assertThat(range.isBefore(new Range(5,8,null)), is(true));
        assertThat(range.isBefore(new Range(5,10,null)), is(false));
        assertThat(range.isBefore(new Range(5,15,null)), is(false));
        assertThat(range.isBefore(new Range(5,50,null)), is(false));
        assertThat(range.isBefore(new Range(5,65,null)), is(false));
        assertThat(range.isBefore(new Range(10,30,null)), is(false));
        assertThat(range.isBefore(new Range(45,50,null)), is(false));
        assertThat(range.isBefore(new Range(45,60,null)), is(false));
        assertThat(range.isBefore(new Range(50,60,null)), is(false));
        assertThat(range.isBefore(new Range(55,60,null)), is(false));
    }

    @Test
    public void isAfter() throws Exception {
        assertThat(range.isAfter(new Range(51,60,null)), is(true));
        assertThat(range.isAfter(new Range(50,60,null)), is(false));
        assertThat(range.isAfter(new Range(45,60,null)), is(false));
        assertThat(range.isAfter(new Range(10,60,null)), is(false));
        assertThat(range.isAfter(new Range(15,60,null)), is(false));
        assertThat(range.isAfter(new Range(20,30,null)), is(false));
        assertThat(range.isAfter(new Range(10,30,null)), is(false));
        assertThat(range.isAfter(new Range(5,30,null)), is(false));
        assertThat(range.isAfter(new Range(5,10,null)), is(false));
        assertThat(range.isAfter(new Range(5,9,null)), is(false));
    }

    @Test
    public void isConcurrent() throws Exception {
        assertThat(range.isConcurrent(new Range(51,60,null)), is(false));
        assertThat(range.isConcurrent(new Range(50,60,null)), is(true));
        assertThat(range.isConcurrent(new Range(45,60,null)), is(true));
        assertThat(range.isConcurrent(new Range(10,60,null)), is(true));
        assertThat(range.isConcurrent(new Range(15,60,null)), is(true));
        assertThat(range.isConcurrent(new Range(20,30,null)), is(true));
        assertThat(range.isConcurrent(new Range(10,30,null)), is(true));
        assertThat(range.isConcurrent(new Range(5,30,null)), is(true));
        assertThat(range.isConcurrent(new Range(5,10,null)), is(true));
        assertThat(range.isConcurrent(new Range(5,9,null)), is(false));
    }

    @Test
    public void contains() throws Exception {
        assertThat(range.contains(2l), is(false));
        assertThat(range.contains(60l), is(false));
        assertThat(range.contains(10l), is(true));
        assertThat(range.contains(50l), is(true));
        assertThat(range.contains(30l), is(true));
    }

    @Test
    public void asList() throws Exception {
        assertNotNull(range.asList());
    }

    @Test
    public void asIterator() throws Exception {
        assertNotNull(range.asIterator());
    }

}